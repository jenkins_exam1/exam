const mysql = require('mysql')
const openConnection = () => {
  const connection = mysql.createConnection({
    host: 'localhost',
    user: 'root',
    password: 'manager',
    database: 'sdmExam',
  })
  connection.connect()
  return connection
}
module.exports = {
  openConnection,
}
