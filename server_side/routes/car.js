const express = require('express')
const db = require('../db')
const utils = require('../utils')
const router = express.Router()

router.post('/post', (request, response) => {
  const { car_name, company_name, car_prize } = request.body
  const statement = `INSERT INTO car_tb (car_name, company_name, car_prize) VALUES ('${car_name}', '${company_name}', ${car_prize})`
  const connection = db.openConnection()
  connection.query(statement, (error, result) => {
    connection.end()
    response.send(utils.createResult(error, result))
  })
})

router.get('/getCar/:car_id', (request, response) => {
  const { car_id } = request.params
  const statement = `SELECT * FROM car_tb where car_id = ${car_id}`
  const connection = db.openConnection()
  connection.query(statement, (error, result) => {
    connection.end()
    response.send(utils.createResult(error, result))
  })
})

router.put('/update', (request, response) => {
  const { company_name, car_prize, car_id } = request.body
  const statement = `UPDATE car_tb SET company_name = '${company_name}', car_prize = ${car_prize} where car_id = ${car_id}`
  const connection = db.openConnection()
  connection.query(statement, (error, result) => {
    connection.end()
    response.send(utils.createResult(error, result))
  })
})

router.delete('/delete/:id', (request, response) => {
  const { id } = request.params
  const statement = `DELETE from car_tb where car_id = ${id}`
  const connection = db.openConnection()
  connection.query(statement, (error, result) => {
    connection.end()
    response.send(utils.createResult(error, result))
  })
})
module.exports = router
