const express = require('express')
const cors = require('cors')
const app = express()
const routerCar = require('./routes/car')

app.use(cors('*'))
app.use(express.json())
app.use('/car', routerCar)
app.listen(3000, '0.0.0.0', () => {
  console.log(`Server Started on port 3000`)
})
